/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermdoll;

/**
 *
 * @author nymr3kt
 */
/*keyword entends เป็นคำสั่งการสืบทอดเพื่อบ่งบอกว่า  class Familyที่เป็นคลาสหลัก 
จะสืบทอดไปที่ class Dog ที่เป็นคลาสลูก*/
public class Dog extends Family{
    /*สร้าง constructor เพื่อกำหนดค่าเริ่มต้นให้กับออบเจ็กต์ก่อนถูกเรียกใช้งาน จะไม่มีการคืนค่า */
    public Dog(String name, String bornnation, String color){
        super(name, bornnation, color, "Dog", 4);
         /*ใช้คำสั่ง super ใน constructor ของคลาสย่อยในคำสั่ง super(name, bornnation, color, "Dog", 4)
        เพราะมามันมีการทำงานเหมือนคลาสแม่ เราไม่จำเป็นต้องเขียนใหม่*/
    }

  
   /*Override เป็นคำสั่งที่ใช้บอกว่า เราเอาmethod จากคลาสแม่มาใช้นะ 
    และOverride ยังสามารถแก้ไขหรือเพิ่มคำสั่งใน Method เพิ่มจากเดิมได้*/
    @Override
    public void introduce(){
        super.introduce();
    }
    @Override
    public void ability(){
        super.ability(); /*คำสั่ง super สามารถเข้าถึงสมาชิกหรือ constructor ของคลาสแม่ได้*/
        System.out.println("I can sniff.");
        System.out.println("I can bark.");
    }
    
}
