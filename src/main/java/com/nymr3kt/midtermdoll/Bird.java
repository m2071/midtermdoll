/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermdoll;

/**
 *
 * @author nymr3kt
 */
public class Bird extends Family{
    private int wing = 2 ;
    public Bird(String name, String bornnation, String color ){
        super(name,bornnation,color,"Bird",2);
        
    }
    
    @Override
    public void introduce(){
        super.introduce();
    }
    
    
    @Override
    public void ability(){
        super.ability();
        System.out.println("I can fly anywhere because I have"+wing+" wings");
        System.out.println("I have a beautiful voice");
    }
}
