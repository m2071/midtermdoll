/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermdoll;

/**
 *
 * @author nymr3kt
 */
public class Family { /* การสร้างคลาสต้นแบบ หรือคลาสแม่ เพื่ิอให้คลาสลูกมาใช้ */
/*  การประการสร้างตัวแปร โดยกำหนดระดับ access modifier เป็น protected 
    ให้กับแอตตรทริบิวต์หรือเมธอดที่มีความสัมพันธ์คลาสแม่คลาสลูกใช้เท่านั้นและไม่อนุญาตให้คลาสอื่นเรียกใช้
    */
    
    protected String name; 
    protected String bornnation;
    protected String color;
    protected char work;
    protected String type;
    protected int numberOfLegs;
/*สร้าง constructor เพื่อกำหนดค่าเริ่มต้นให้กับออบเจ็กต์ก่อนถูกเรียกใช้งาน จะไม่มีการคืนค่าและไม่ต้องมีคีย์เวิร์ด viod */
    public Family(String name, String bornnation, String color, 
            String type, int numberOfLegs) {
        /* */
        this.name = name ;
        this.bornnation = bornnation ;
        this.color = color ;
        this.type = type ;
        this.numberOfLegs = numberOfLegs ;
        this.work = work ;
       
    }
/* การสร้าง method เพื่อสร้างคำสั่งการทำงาน
    ในที่นี้มีคีย์เวิร์ด void ซึ่งบ่งบอกว่า method นี้ไม่มีการรับพารามิตเตอร์และไม่มีการคืนค่า*/
    public void introduce() {
        System.out.println("-------------------Introduce Myself-----------------");
        System.out.println("Hi My name is " + this.name
                + " Born National : " + this.bornnation);
        System.out.println("My color : " + this.color + " My type is : " 
                + this.type );
        System.out.println("I have " + this.numberOfLegs + " of legs");
        
    }

    public void ability() {
        System.out.println("-----------------My ability-------------------");
    }
    public void end() {
        System.out.println("-------------------------------------------------");
        System.out.println();
    }

}
