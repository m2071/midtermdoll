/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermdoll;

/**
 *
 * @author nymr3kt
 */

public class MainProject {
    public static void main(String[] args) {
        
        Dog snopy = new Dog("Snopy","USA","White");   /* สร้างออบเจ็กต์ จากคลาส Dog*/
        snopy.introduce(); /*เป็นการเรียกใช้งานเมธอดของคลาสนั้นๆ*/
        snopy.ability();
        snopy.end();
        
        Panguin gavin = new Panguin("Gavin","North Pole","Blue & White");  /* สร้างออบเจ็กต์ จากคลาส Panguin*/
        gavin.introduce();
        gavin.ability();/*เป็นการเรียกใช้งานเมธอดของคลาสนั้นๆ*/
        gavin.end();
        
        Bird bigbird = new Bird("BigBird","LA","Yellow");  /* สร้างออบเจ็กต์ จากคลาส Bird*/
        bigbird.introduce();
        bigbird.ability();
        bigbird.end();/*เป็นการเรียกใช้งานเมธอดของคลาสนั้นๆ*/
        
        Mouse jerry = new Mouse("Jerry","USA","Brown");  /* สร้างออบเจ็กต์ จากคลาส Mouse*/
        jerry.introduce();
        jerry.ability();/*เป็นการเรียกใช้งานเมธอดของคลาสนั้นๆ*/
        jerry.end();
        
        Cookie cookie = new Cookie("Cookie","LA","Blue");  /* สร้างออบเจ็กต์ จากคลาส Cookie*/
        cookie.introduce();
        cookie.ability();
        cookie.end();/*เป็นการเรียกใช้งานเมธอดของคลาสนั้นๆ*/
                
    }
}
