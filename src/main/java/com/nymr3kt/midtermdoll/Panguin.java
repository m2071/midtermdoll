/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nymr3kt.midtermdoll;

/**
 *
 * @author nymr3kt
 */
/*keyword entends เป็นคำสั่งการสืบทอดเพื่อบ่งบอกว่า  class Familyที่เป็นคลาสหลัก 
จะสืบทอดไปที่ class Dog ที่เป็นคลาสลูก*/

public class Panguin extends Family {
    private int wings = 2 ;    /*การประกาศตัวแปร ซึ่งระดับการเข้าถึงของตัวแปรนี้คือ private 
    จะไม่มีคลาสไหนสามารถเข้าถึงตัวแปรนี้ได้*/ 
     /*สร้าง constructor เพื่อกำหนดค่าเริ่มต้นให้กับออบเจ็กต์ก่อนถูกเรียกใช้งาน จะไม่มีการคืนค่า */
    public Panguin(String name, String bornnation, String color) {
        super(name, bornnation, color, "Panguin", 2);

    }

    @Override
    public void introduce() {
        super.introduce();
    }

    @Override
    public void ability() {
        super.ability();
        System.out.println("I have "+wings+
                " of wings but can not use wings to fly like other birds");
        System.out.println("I can swim very fast and fluently.");
        
    }

}
